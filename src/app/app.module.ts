import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { StudentService } from './service/student-service';
// import { StudentDataImplService } from './service/student-data-impl.service';
import { StudentFileImplService } from './service/student-file-impl.service';
import { StudentMyImplService } from './service/student-my-impl.service';

import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    // {provide: StudentService, useClass: StudentDataImplService}
    {provide: StudentService, useClass: StudentFileImplService }
    // {provide: StudentService, useClass: StudentMyImplService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
