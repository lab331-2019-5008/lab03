import { TestBed } from '@angular/core/testing';

import { StudentMyImplService } from './student-my-impl.service';

describe('StudentMyImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentMyImplService = TestBed.get(StudentMyImplService);
    expect(service).toBeTruthy();
  });
});
